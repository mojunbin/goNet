package code

import (
	"GoNet/core"
	"fmt"

	"code.google.com/p/goprotobuf/proto"
)

type ClientPbEncodeFilter struct {
	*core.Filter
}

//写
func (f *ClientPbEncodeFilter) MessageWrite(session *core.Session, obj interface{}, serverEngin *core.ServerEngin) {
	message := obj.(*core.Message)
	bodyBytes, err := proto.Marshal(message.Body)
	if err != nil {
		panic(err)
	}
	fmt.Println(len(bodyBytes))

	session.OutBuff.InitWriteBuf(0)
	session.OutBuff.WriteInt32BE(int32(len(bodyBytes) + 2))
	session.OutBuff.WriteUint16BE(uint16(message.Type))
	session.OutBuff.Write(bodyBytes)
	session.Conn.Write(session.OutBuff.Data)
}
