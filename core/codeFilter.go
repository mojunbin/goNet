package core

type CodeFilter struct {
	Filter
	decode IDecode
	encode IEncode
}

func (f *CodeFilter) MessageRecieved(session *Session, obj interface{}, serverEngin *ServerEngin) {
	f.decode.MessageRecieved(session, serverEngin)
}

func (f *CodeFilter) MessageWrite(session *Session, obj interface{}, serverEngin *ServerEngin) {
	f.encode.MessageWrite(session, obj, serverEngin)
}

type IDecode interface {
	MessageRecieved(session *Session, serverEngin *ServerEngin)
}

type IEncode interface {
	MessageWrite(session *Session, obj interface{}, serverEngin *ServerEngin)
}
